PIXI = require "pixi.js"
LoadingState = require "./states/loading.coffee"
PlayState = require "./states/play.coffee"

class Game
    stage: null
    superstage: null
    renderer: null

    transitionOverlay: null
    transitionActive: 0
    TRANSITION_DURATION = 0.25

    height: 0
    width: 0

    TARGET_WIDTH = 160
    TARGET_HEIGHT = 240

    constructor: ->
        @stage = new PIXI.Container
        @superstage = new PIXI.Container
        @superstage.addChild @stage
        @renderer = PIXI.autoDetectRenderer 1, 1,
            autoResize: true,
            resolution: window.devicePixelRatio
        @renderer.backgroundColor = 0x639663
        @renderer.plugins.interaction.autoPreventDefault = false

        do @handleResize

        window.document.body.appendChild @renderer.view

        @keys = new Array(256)
        @keys[i] = false for i in [0..256]

        # bind events
        window.addEventListener "keydown", @handleKeydown.bind @
        window.addEventListener "keyup", @handleKeyup.bind @
        window.addEventListener "blur", @handleBlur.bind @
        window.addEventListener "resize", @handleResize.bind @

        # prepare the first state
        @state = new LoadingState @, PlayState
        @nextState = null
        do @state.enter

    run: ->
        @lastUpdate = do Date.now

        # run the game
        @boundUpdate = @update.bind @
        do @boundUpdate

    changeState: (newState) ->
        @transitionActive = TRANSITION_DURATION * 2
        @nextState = newState

    update: ->
        dt = (do Date.now - @lastUpdate) / 1000
        @lastUpdate = do Date.now
        window.requestAnimationFrame @boundUpdate

        # process transition if in progress
        if @transitionActive
            if @transitionActive < TRANSITION_DURATION
                @transitionOverlay.alpha = @transitionActive / TRANSITION_DURATION
                if @nextState
                    do @state.leave
                    @state = @nextState
                    @nextState = null
                    do @state.enter
            else
                @transitionOverlay.alpha = 1 - (@transitionActive - TRANSITION_DURATION) / TRANSITION_DURATION
            @transitionActive -= dt

            if @transitionActive < 0
                @transitionActive = 0
        else
            @transitionOverlay.alpha = 0
            @state.update dt

        @renderer.render @superstage

    boundUpdate: null

    handleKeydown: (e) ->
        @keys[e.which] = true

    handleKeyup: (e) ->
        @keys[e.which] = false

    handleBlur: (e) ->
        @keys[i] = false for i in [0 .. 256]

    handleResize: (e) ->
        ratio = Math.floor Math.min(window.innerWidth / TARGET_WIDTH, window.innerHeight / TARGET_HEIGHT)
        if ratio < 1
            ratio = 1

        # provide the unscaled size for states
        @width = window.innerWidth / ratio
        @height = window.innerHeight / ratio

        # recreate the transition overlay
        @superstage.removeChild @transitionOverlay
        @transitionOverlay = new PIXI.Graphics
        @transitionOverlay.beginFill 0x000000
        @transitionOverlay.drawRect 0, 0, innerWidth, innerHeight
        @transitionOverlay.alpha = 0
        @superstage.addChild @transitionOverlay

        # update the canvas size and scale
        @renderer.resize window.innerWidth, window.innerHeight
        @stage.scale.set ratio

        # allow the state to scale itself (if any)
        do @state.resize if @state



module.exports = Game
