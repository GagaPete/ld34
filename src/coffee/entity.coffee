PIXI = require "pixi.js"
settings = require "./settings.coffee"

class Entity
    x: 0
    y: 0

    constructor: ->
        @view = new PIXI.Container
        @view.scale.set settings.gapFixScale

    think: ->

    isSolid: ->
        return false

module.exports = Entity
