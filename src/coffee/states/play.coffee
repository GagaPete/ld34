PIXI = require "pixi.js"
settings = require "../settings.coffee"
EndingState = require "../states/ending.coffee"
Player = require "../entities/player.coffee"
PickableAction = require "../entities/pickableAction.coffee"
Door = require "../entities/door.coffee"
DoorSwitch = require "../entities/doorSwitch.coffee"
Exit = require "../entities/exit.coffee"

class PlayState
    player: null
    width: null
    height: null

    constructor: (@game, @levelName = 1) ->
        @camera = new PIXI.Container
        @level = PIXI.loader.resources.levels.data[@levelName]
        @doorUnlocked = new Array

        @actionbar = new PIXI.Container
        @actionbarSprite = new PIXI.Sprite PIXI.Texture.fromFrame "actionbar.png"
        @actionbar.addChild @actionbarSprite

        # sprites to display the actions
        @action1Sprite = new PIXI.Sprite
        @action1Sprite.position.x += 14
        @action1Sprite.interactive = true
        @action1Sprite.on "tap", =>
            @player.do @actions[0]
        @action1Sprite.on "click", =>
            @player.do @actions[0]
        @actionbar.addChild @action1Sprite
        @action2Sprite = new PIXI.Sprite
        @action2Sprite.position.x += 33
        @action2Sprite.interactive = true
        @action2Sprite.on "tap", =>
            @player.do @actions[1]
        @action2Sprite.on "click", =>
            @player.do @actions[1]
        @actionbar.addChild @action2Sprite

        # replay button
        @replaySprite = new PIXI.Sprite PIXI.Texture.fromFrame "replay.png"
        @replaySprite.interactive = true
        @replaySprite.on "tap", =>
            @game.changeState new PlayState @game, @levelName
        @replaySprite.on "click", =>
            @game.changeState new PlayState @game, @levelName

        @_parseJson @level

    enter: ->
        @game.stage.addChild @camera
        @game.stage.addChild @actionbar
        @game.stage.addChild @replaySprite
        do @resize

    leave: ->
        @game.stage.removeChild @camera
        @game.stage.removeChild @actionbar
        @game.stage.removeChild @replaySprite

    update: (dt) ->
        entity.think dt for entity in @entities when entity != @player

        if @game.keys[settings.key_action_1.code]
            @player.do @actions[0]
        if @game.keys[settings.key_action_2.code]
            @player.do @actions[1]

        @player.think dt

        # lazily remove removed enemies ;)
        for entity in @removedEntities
            index = @entities.indexOf entity
            if index != -1
                @camera.removeChild entity.view
                @entities.splice index, 1

        # camera should follow player
        @camera.position.set -@player.view.x, -@player.view.y

        if @game.keys[settings.key_reset.code]
            @game.changeState new PlayState @game, @levelName

    resize: ->
        @camera.pivot.set @game.width / -2, @game.height / -2
        @camera.position.set -@player.view.x, -@player.view.y

        @actionbar.position.set (@game.width / 2) - 32, @game.height - 20
        @replaySprite.position.set 4, @game.height - 20

    collectAction: (action) ->
        switch action
            when "up" then @actions[0] = action
            when "down" then @actions[0] = action
            when "left" then @actions[1] = action
            when "right" then @actions[1] = action

        @action1Sprite.texture = PIXI.Texture.fromFrame "action_" + @actions[0] + ".png"
        if @actions[1]
            @action2Sprite.visible = true
            @action2Sprite.texture = PIXI.Texture.fromFrame "action_" + @actions[1] + ".png"
        else
            @action2Sprite.visible = false

    isSolid: (x, y) ->
        result = x < 0 or y < 0 or x >= @width or y >= @height
        if not result
            result = !!@solid[x + y * @width]
            if not result
                for entity in @entities
                    if entity.x == x and entity.y == y and do entity.isSolid
                        result = true

        result

    unlockDoor: (color) ->
        @doorUnlocked.push color

    isDoorUnlocked: (color) ->
        color in @doorUnlocked

    solved: ->
        if @levelName + 1 of PIXI.loader.resources.levels.data
            @game.changeState new PlayState @game, @levelName + 1
        else
            @game.changeState new EndingState @game

    addEntity: (entity) ->
        @entities.push entity
        @camera.addChild entity.view

    removeEntity: (entity) ->
        @removedEntities.push entity

    _parseJson: (obj) ->
        @width = obj.width
        @height = obj.height
        @pixelWidth = obj.width * settings.tileSize
        @pixelHeight = obj.height * settings.tileSize

        # create lists
        @actions = new Array 2
        @tiles = new Object
        @solid = new Array obj.width * obj.height
        @entities = new Array
        @removedEntities = new Array

        # parse tiles
        @tiles = new Object
        for tileset in obj.tilesets
            for id, image of tileset.tiles
                @tiles[tileset.firstgid + parseInt(id)] = image.image.replace(/^..\/image\//, '')

        # parse layers
        @layers = new Array
        for layer in obj.layers
            if layer.type == "tilelayer" and /^entities/.test layer.name
                # entity layers, contains spawners
                container = new PIXI.Container
                for i in [0...layer.data.length]
                    if @tiles[layer.data[i]]
                        x = i % @width
                        y = Math.floor i / @width
                        switch @tiles[layer.data[i]]
                            when "player_up_idle.png"
                                @player = new Player @, x, y
                                @addEntity @player
                            when "action_up.png"
                                action = new PickableAction @, x, y, "up"
                                @addEntity action
                            when "action_down.png"
                                action = new PickableAction @, x, y, "down"
                                @addEntity action
                            when "action_left.png"
                                action = new PickableAction @, x, y, "left"
                                @addEntity action
                            when "action_right.png"
                                action = new PickableAction @, x, y, "right"
                                @addEntity action
                            when "door_green.png"
                                door = new Door @, x, y, "green"
                                @addEntity door
                            when "door_blue.png"
                                door = new Door @, x, y, "blue"
                                @addEntity door
                            when "door_red.png"
                                door = new Door @, x, y, "red"
                                @addEntity door
                            when "doorswitch_green_inactive.png"
                                door = new DoorSwitch @, x, y, "green"
                                @addEntity door
                            when "doorswitch_blue_inactive.png"
                                door = new DoorSwitch @, x, y, "blue"
                                @addEntity door
                            when "doorswitch_red_inactive.png"
                                door = new DoorSwitch @, x, y, "red"
                                @addEntity door
                            when "exit.png"
                                exit = new Exit @, x, y
                                @addEntity exit

            else if layer.type == "tilelayer"
                # every other tile layer
                container = new PIXI.Container
                for x in [0...@width]
                    for y in [0...@height]
                        if @tiles[layer.data[x + y * @width]]
                            sprite = new PIXI.Sprite PIXI.Texture.fromFrame @tiles[layer.data[x + y * @width]]
                            sprite.position.set settings.tileSize * x, settings.tileSize * y
                            sprite.scale.set settings.gapFixScale
                            container.addChild sprite

                            if layer.name == "walls"
                                @solid[x + y * @width] = true
                        else if layer.name == "floor"
                            @solid[x + y * @width] = true
                @camera.addChild container

        # parse properties
        if "action_1" of obj.properties
            @collectAction obj.properties.action_1
        if "action_2" of obj.properties
            @collectAction obj.properties.action_2

module.exports = PlayState
