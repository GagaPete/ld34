PIXI = require "pixi.js"

class LoadingState
    completed: false

    constructor: (@game, @nextState) ->

    enter: ->
        @intro = PIXI.Sprite.fromImage "./logo.png"
        @game.stage.addChild @intro
        @state = PIXI.Sprite.fromImage "./loading.png"
        @game.stage.addChild @state

        PIXI.loader.add "spritesheet", "./spritesheet.json"
        PIXI.loader.add "levels", "./levels.json"
        PIXI.loader.load => @completed = true

    leave: ->
        game.stage.removeChild @intro
        game.stage.removeChild @state

    update: ->
        if @completed
            @state.texture = PIXI.Texture.fromFrame "start.png"
            if not @state.interactive
                @state.interactive = true
                @state.on "tap", =>
                    @game.changeState new @nextState @game
                @state.on "click", =>
                    @game.changeState new @nextState @game

        do @resize

    resize: ->
        @intro.position.set (@game.width - 148) / 2, (@game.height - 56) / 2
        @state.position.set (@game.width - @state.texture.width) / 2, (@game.height - 56) / 2 + 28 + 7

module.exports = LoadingState
