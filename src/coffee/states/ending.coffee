PIXI = require "pixi.js"

class EndingState
    constructor: (@game, @nextState) ->

    enter: ->
        @end = new PIXI.Sprite PIXI.Texture.fromFrame "end.png"
        @game.stage.addChild @end

        do @resize

    leave: ->
        game.stage.removeChild @end

    update: ->

    resize: ->
        @end.position.set (@game.width - @end.texture.width) / 2, (@game.height - @end.texture.height) / 2

module.exports = EndingState
