PIXI = require "pixi.js"
settings = require "../settings.coffee"
Entity = require "../entity.coffee"

class PickableAction extends Entity
    locked: false

    constructor: (@playState, @x, @y, @action) ->
        super

        @view.addChild new PIXI.Sprite PIXI.Texture.fromFrame "action_" + @action + ".png"
        @view.position.set @x * settings.tileSize, @y * settings.tileSize

    think: ->
        if @playState.player.x == @x and @playState.player.y == @y
            if not @locked
                @playState.collectAction @action
                @locked = true
        else
            @locked = false

module.exports = PickableAction
