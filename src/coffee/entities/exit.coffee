PIXI = require "pixi.js"
settings = require "../settings.coffee"
Entity = require "../entity.coffee"

class Exit extends Entity
    constructor: (@playState, @x, @y) ->
        super

        @view.addChild new PIXI.Sprite PIXI.Texture.fromFrame "exit.png"
        @view.position.set @x * settings.tileSize, @y * settings.tileSize

    think: ->
        if @playState.player.x == @x and @playState.player.y == @y
            do @playState.solved

module.exports = Exit
