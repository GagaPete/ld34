PIXI = require "pixi.js"
settings = require "../settings.coffee"
Entity = require "../entity.coffee"

class Player extends Entity
    transition: 0
    TRANSITION_DURATION = 0.3

    constructor: (@playState, @x, @y) ->
        @view = new PIXI.Container

        @sprite = new PIXI.Sprite PIXI.Texture.fromFrame "player_up_idle.png"
        @sprite.scale.set settings.gapFixScale
        @view.addChild @sprite
        @view.position.set @x * settings.tileSize, @y * settings.tileSize - settings.tileSize * 0.25
        @nextX = @x
        @nextY = @y

    do: (action) ->
        if @x == @nextX and @y == @nextY
            switch action
                when "up" then @nextY -= 1
                when "down" then @nextY += 1
                when "left" then @nextX -= 1
                when "right" then @nextX += 1
            if @playState.isSolid @nextX, @nextY
                @nextX = @x
                @nextY = @y
            else if @nextX != @x or @nextY != @y
                @transition = TRANSITION_DURATION

    think: (dt) ->
        if @transition <= 0
            @x = @nextX
            @y = @nextY
            @view.position.set @x * settings.tileSize, @y * settings.tileSize - settings.tileSize * 0.25
            @transition = 0
        else if @transition
            @view.position.x = (@x + (@nextX - @x) * (1 - @transition / TRANSITION_DURATION)) * settings.tileSize
            @view.position.y = (@y + (@nextY - @y) * (1 - @transition / TRANSITION_DURATION)) * settings.tileSize - settings.tileSize * 0.25
            @transition -= dt


module.exports = Player
