PIXI = require "pixi.js"
settings = require "../settings.coffee"
Entity = require "../entity.coffee"

class Door extends Entity
    constructor: (@playState, @x, @y, @color) ->
        super

        @view.addChild new PIXI.Sprite PIXI.Texture.fromFrame "door_" + @color + ".png"
        @view.position.set @x * settings.tileSize, @y * settings.tileSize - 0.75 * settings.tileSize

    think: ->
        if @playState.isDoorUnlocked @color
            @view.position.set @x * 16, @y * 16

    isSolid: ->
        return not @playState.isDoorUnlocked @color

module.exports = Door
