PIXI = require "pixi.js"
settings = require "../settings.coffee"
Entity = require "../entity.coffee"

class DoorSwitch extends Entity
    constructor: (@playState, @x, @y, @color) ->
        super

        @view.addChild new PIXI.Sprite PIXI.Texture.fromFrame "doorswitch_" + @color + "_inactive.png"
        @view.position.set @x * settings.tileSize, @y * settings.tileSize

    think: ->
        if @playState.player.x == @x and @playState.player.y == @y
            @playState.unlockDoor @color
            @view.addChild new PIXI.Sprite PIXI.Texture.fromFrame "doorswitch_" + @color + "_active.png"

module.exports = DoorSwitch
