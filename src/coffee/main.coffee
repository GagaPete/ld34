Game = require "./game.coffee"

# I use a pixelated style, so scale pixelated
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST

window.addEventListener "DOMContentLoaded", ->
    window.game = new Game
    do window.game.run
