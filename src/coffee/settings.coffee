module.exports =
    key_action_1:
        name: "X"
        code: 88

    key_action_2:
        name: "C"
        code: 67

    key_reset:
        name: "R"
        code: 82

    gapFixScale: 1.006
    tileSize: 16
