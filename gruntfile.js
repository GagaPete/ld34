module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-image');
    grunt.loadNpmTasks('grunt-jsonmin');
    grunt.loadNpmTasks('grunt-merge-data');
    grunt.loadNpmTasks('grunt-texturepacker');

    grunt.initConfig ({
        browserify: {
            compile: {
                files: {
                    'temp/main.js': ['src/coffee/**/*.coffee']
                },
                options: {
                    transform: ['coffeeify']
                }
            }
        },

        copy: {
            dist: {
                expand: true,
                src: ['*.html'],
                dest: 'dist/',
                filter: 'isFile',
                cwd: 'src/'
            },
            dist2: {
                expand: true,
                src: ['logo.png', 'loading.png'],
                dest: 'dist/',
                cwd: 'src/image/',
            }
        },

        uglify: {
            min: {
                files: {
                    'dist/main.js' : ['temp/main.js']
                }
            }
        },

        texturepacker: {
            dist: {
                files: {
                    src: 'src/image/**/*.png'
                },
                options: {
                    output: {
                        sheet: {
                            file: 'temp/spritesheet.png',
                            format: 'png'
                        },
                        data: {
                            file: 'temp/spritesheet.json',
                            format: 'phaser-json-hash'
                        }
                    },
                    algorithm: 'Basic',
                    'trim-mode': 'None',
                    'png-opt-level': 0
                }
            }
        },

        merge_data: {
            dist: {
                src: 'src/level/*.json',
                dest: 'temp/levels.json'
            }
        },

        image: {
            dist: {
                files: {
                    'dist/spritesheet.png': 'temp/spritesheet.png'
                }
            }
        },

        jsonmin: {
            dist: {
                options: {
                    stripWhitespace: true,
                    strioComments: true
                },
                files: {
                    'dist/spritesheet.json': 'temp/spritesheet.json',
                    'dist/levels.json': 'temp/levels.json',
                }
            }
        },

        watch: {
            options: { livereload: true }, // reloads browser on save
            html: {
                files: ['src/*.html', 'src/loading.png', 'src/logo.png'],
                tasks: ['copy:dist']
            }, //html
            scripts: {
                files: ['src/coffee/**/*.coffee'],
                tasks: ['browserify:compile', 'uglify']
            }, //scripts
            merge_data: {
                files: ['src/level/*.json'],
                tasks: ['merge_data','jsonmin']
            }, //merge_data
            texturepacker: {
                files: ['src/image/**/*.png'],
                tasks: ['texturepacker:dist']
            }, //texturepacker
            image: {
                files: ['temp/spritesheet.png'],
                tasks: ['image:dist'],
            }, //image
            perma_image: {
                files: ['src/image/logo.png','src/image/loading.png'],
                tasks: ['copy:dist2'],
            }, //perma_image
            jsonmin: {
                files: ['temp/*.json'],
                tasks: ['jsonmin:dist'],
            }, //jsonmin
        } //watch
    }) //initConfig
    grunt.registerTask('default', 'watch');
    // TODO: Release task (change version, git-tag, publish to webspace)
} //exports
